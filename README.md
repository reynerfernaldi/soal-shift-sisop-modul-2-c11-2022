# Soal-Shift-Sisop-Modul-2-C11-2022

## Daftar Isi ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomer 1](#nomer-1)
    - [Soal  1.A](#soal-1a)
    - [Soal  1.B](#soal-1b)
    - [Soal  1.C](#soal-1c)
    - [Soal  1.D](#soal-1d)
    - [Soal  1.E](#soal-1e)
- [Nomer 2](#nomer-2)
    - [Soal  2.A](#soal-2b)
    - [Soal  2.B](#soal-2b)
    - [Soal  2.C](#soal-2c)
    - [Soal  2.D](#soal-2d)
    - [Soal  2.E](#soal-2e)
- [Referensi](#referensi)   
- [Kesulitan](#kesulitan)


## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201094  | Reyner Fernaldi | SISOP C
505201030  | Mohammad Nouval Bachrezi | SISOP C

## Nomer 1 ##
### Soal 1.a ###
### Soal 1.b ###
### Soal 1.c ###
### Soal 1.d ###
### Soal 1.e ###

## Nomer 2 ##
Yang pertama saya lakukan adalah membuat fungsi proses
```c
void process(char bin[], char *arg[]) {
    int status;
    pid_t child_id = fork();
    if (child_id == 0)
        execv(bin, arg);
    else
        ((wait(&status)) > 0);
}
```
Fungsi ini akan digunakan untuk membuat proses lain seperti buat direktori, unzip, copy, dan delete
### Soal 2.a ###
Mengekstrak zip ke directory `“home/[user]/shift2/drakor` dilakukan dengan membuat fungsi `makedir`

```c
void makedir(char *dest) {
	char *arg[] = {"mkdir", "-p", dest, NULL};
	process("/usr/bin/mkdir", arg);
}
```
Fungsi ini akan menjalankan proses `mkdir` sesuai dengan destination yang ditentukan.
Kemudian mengextract zip dengan menggunakan fungsi `unzip`

```c
void unzip(char *dest) {
	char *arg[] = {"unzip", "-j", "drakor.zip", "*.png", "-d", dest, NULL};
	process("/usr/bin/unzip", arg);
}
```
Fungsi `unzip` hanya akan mengextract file yang berekstensi ".png". Hal ini digunakan agar sistem tidak mengekstrak file yang tidak dibutuhkan.

### Soal 2.b ###
Membuat folder sesuai kategori.
Yang pertama harus dilakukan adalah mengambil kategori apa saja yang ada di dalam poster. Cara yang saya gunakan adalah dengan mengambil nama akhir dari setiap file, kemudian gunakan nama tersebut sebagai kategori poster
```c
        char tempChar[1000];
		char tempFolder[1000];

		strcpy(tempFolder, dp->d_name);
		char *last = strrchr(tempFolder, ';');
		if (last != NULL) {
			strcpy(tempChar, last+1); // mengambil string paling belakang
			strtok(tempChar, "."); // membuang ".png"
		    strcat(mkFolder, tempChar);
		}

        makedir(mkFolder);

```
### Soal 2.c ###
Memindahkan poster ke folder masing-masing.
Untuk memindahkannya saya membuat fungsi `copy` dan `delete`. Kenapa tidak menggunakan `rm`? Hal ini dikarenakan terdapat file yang sama yang harus dipindahkan kedalam 2 atau lebih file. Hal ini tidak dimungkinkan dengan perintah `rm`
```c
void copy(char *src, char *dest) {
	char *arg[] = {"cp", "-n", src, dest, NULL};
	process("/usr/bin/cp", arg);
}

void delete (char *file) {
	char *arg[] = {"rm", "-r", file, NULL};
	process("/usr/bin/rm", arg);
}

```
Setelah itu, saya akan memisahkan keterangan dari setiap poster kedalam variabel-variabel, diamana variabel tersebut akan digunakan untuk kebutuhan-kebutuhan selanjutnya, salah satunya me-rename file yang dipindahkan.
```c
		copy(oriFiles, posterSatu); // 1 gambar 1 poster

```
setelah dicopy, file asli akan dihapus
```c
		delete(oriFiles);
```
### Soal 2.d ###
Karena ada gambar yang terdiri dari 2 poster, maka kita harus memisahkan nama poster tesebut menjadi 2, kemudian masing-masing nama poster dipindahkan ke folder masing masing.
```c
		copy(oriFiles, posterSatu); // 1 gambar 1 poster
		copy(oriFiles, posterDua);  // 2 gambar 1 poster (gambar pertama)
		copy(oriFiles, posterTiga); // 2 gambar 1 poster (gambar kedua)
```
### Soal 2.e ###
Untuk membuat data.txt, saya menggunakan C FILE.
```c
      		if ((!(strstr(dp->d_name, "_"))) && (strstr(dp->d_name, ".png"))) {  // untuk 1 poster
			FILE *fptr;
			char fname[100];
			long size;
			strcpy(fname, mkFolder);
			strcat(fname, "/data.txt");
			fptr = fopen(fname, "r");
			// cek apakah 
			if (fptr){
				fseek(fptr, 0, SEEK_END);
				size = ftell(fptr);	// cek size file txt nya
				fclose(fptr);
			}
			fptr = fopen(fname, "a+");
			if(size == 0){		// jika size nya 0, berarti data masih kosong
				fprintf(fptr, "kategori : %s\n\n", getKategori);
			}
			fprintf(fptr, "nama : %s\n", getJudul);
			fprintf(fptr, "rilis : tahun %s\n\n", getTahun);
			fclose(fptr);
		}
		
		// entah kenapa untuk yang 2 poster saya kena segmentation fault
/*
		else if(strstr(dp->d_name, "_") && (strstr(dp->d_name, ".png"))) {
		        FILE *fptr;
			char fname[100];
			strcpy(fname, oriFiles);
			strcat(fname, getKategori2);
			strcat(fname, "/data.txt");
			fptr = fopen(fname, "a+");
			fprintf(fptr, "nama : %s\n", getJudul2);
			fprintf(fptr, "rilis : tahun %s\n\n", getTahun2);
			fclose(fptr);
				
			strcpy(fname, oriFiles);
			strcat(fname, getKategori3);
			strcat(fname, "/data.txt");
			fptr = fopen(fname, "a+");
			fprintf(fptr, "nama : %s\n", getJudul3);
			fprintf(fptr, "rilis : tahun %s\n\n", getTahun3);
			fclose(fptr);
		}
*/

```
File akan dicek ukurannya. Jika ukuran file 0, maka itu merupakan file baru, sehingga tuliskan kategorinya terlebih dahulu. Selanjutnya isikan nama dan tahun rilisnya
![1](/uploads/8d3813701c3469f274732011415aca5f/1.png)
![2](/uploads/df8fe1fbf90fc3c51111c4d0e01fe756/2.png)
## Referensi ##
Saya menggunakan referensi [ini](https://codeforwin.org/2018/03/c-program-to-list-all-files-in-a-directory-recursively.html) untuk membaca setiap nama poster

## Kesulitan ##
Saya masih belum bisa memasukkan 1 gambar 2 poster ke dalam data.txt. Ketika saya coba, program berhenti dan memunculkan `Segmentation fault (code dumped)`

