#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>


// Buat Process
void process(char bin[], char *arg[]) {
    int status;
    pid_t child_id = fork();
    if (child_id == 0)
        execv(bin, arg);
    else
        ((wait(&status)) > 0);
}
// Buat Directory
void makedir(char *dest) {
	char *arg[] = {"mkdir", "-p", dest, NULL};
	process("/usr/bin/mkdir", arg);
}
// Buat Unzip
void unzip(char *dest) {
	char *arg[] = {"unzip", "-j", "drakor.zip", "*.png", "-d", dest, NULL};
	process("/usr/bin/unzip", arg);
}
// Untuk mengcopy file ke dalam directory (memindahkan)
void copy(char *src, char *dest) {
	char *arg[] = {"cp", "-n", src, dest, NULL};
	process("/usr/bin/cp", arg);
}

// Untuk delete file (memindahkan)
void delete (char *file) {
	char *arg[] = {"rm", "-r", file, NULL};
	process("/usr/bin/rm", arg);
}
// Secara rekursif akan membaca file
void listFilesRecursively(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    // Unable to open directory stream
    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
        	char tempChar[1000];
		char tempFolder[1000];
		char mkFolder[1000] = "/home/reyner/shift2/drakor/";
		char posterSatu[1000] = "/home/reyner/shift2/drakor/";
		char posterDua[1000] = "/home/reyner/shift2/drakor/";
		char posterTiga[1000] = "/home/reyner/shift2/drakor/";
		char temp[1000], temp2[1000], temp3[1000], getJudul[1000], getTahun[1000], getKategori[1000], getJudul2[1000], getTahun2[1000], getKategori2[1000], getJudul3[1000], getTahun3[1000], getKategori3[1000];
		
		//Untuk membuat directory berdasarkan kategori
		strcpy(tempFolder, dp->d_name);
		char *last = strrchr(tempFolder, ';');
		if (last != NULL) {
			strcpy(tempChar, last+1); // mengambil string paling belakang
			strtok(tempChar, "."); // membuang ".png"
		    strcat(mkFolder, tempChar);
		}
			
		char oriFiles[100] = "/home/reyner/shift2/drakor/"; // untuk memindahkan poster
		char *token, *token2, *token3;
		strcat(oriFiles, dp->d_name);
		// Untuk 1 poster dalam 1 gambar
		if ((!(strstr(dp->d_name, "_"))) && (strstr(dp->d_name, ".png"))) {
			strcpy(temp, dp->d_name); //all-of-us-are-dead;2022;thriller.png
			token = strtok(temp, ";"); //all-of-us-are-dead
			strcpy(getJudul, token);
			token = strtok(NULL, ";"); //2022
			strcpy(getTahun, token);
			token = strtok(NULL, ";"); //thriller.png
			token = strtok(token, "."); //thriller
			strcpy(getKategori, token);
			strcat(posterSatu, getKategori); // /home/reyner/shift2/drakor/thriller
			strcat(posterSatu, "/");	// /home/reyner/shift2/drakor/thriller/
			strcat(posterSatu, getJudul);	// /home/reyner/shift2/drakor/thriller/all-of-us-are-dead
			strcat(posterSatu, ".png");	// /home/reyner/shift2/drakor/thriller/all-of-us-are-dead.png
		
		}
		// Untuk 2 poster dalam 1 gambar
		else if (strstr(dp->d_name, "_") && (strstr(dp->d_name, ".png"))) {
			strcpy(temp2, dp->d_name); //love-alarm;2019;romance_who-are-you;2015;school
			token2 = strtok(temp2, "_"); //love-alarm;2019;romance
			token2 = strtok(temp2, ";"); //love-alarm
			strcpy(getJudul2, token2);
			token2 = strtok(NULL, ";"); //2019
			strcpy(getTahun2, token2);
			token2 = strtok(NULL, ";"); //romance
			strcpy(getKategori2, token2);
			strcat(posterDua, getKategori2); // /home/reyner/shift2/drakor/thriller
			strcat(posterDua, "/"); 	// /home/reyner/shift2/drakor/thriller/
			strcat(posterDua, getJudul2);	// /home/reyner/shift2/drakor/thriller/love-alarm
			strcat(posterDua, ".png");	// /home/reyner/shift2/drakor/thriller/love-aram.png

			strcpy(temp3, dp->d_name); //love-alarm;2019;romance_who-are-you;2015;school.png
			token3 = strtok(temp3, "_"); //love-alarm;2019;romance_
			token3 = strtok(NULL, "_");	 //who-are-you;2015;school.png
			strcpy(temp3, token3);
			token3 = strtok(temp3, ";"); //who-are-you
			strcpy(getJudul3, token3);
			token3 = strtok(NULL, ";"); //2015
			strcpy(getTahun3, token3);
			token3 = strtok(NULL, ";"); //school.png
			token3 = strtok(token3, "."); //school
			strcpy(getKategori3, token3);
			strcat(posterTiga, getKategori3);
			strcat(posterTiga, "/");
			strcat(posterTiga, getJudul3);
			strcat(posterTiga, ".png");

		}
		// Buat directory berdasarkan kategorinya
		makedir(mkFolder);
		
		// Copy file asli ke dalam directory yang telah dibuat
		copy(oriFiles, posterSatu);
		copy(oriFiles, posterDua);
		copy(oriFiles, posterTiga);
		
		// Hapus File Original
		delete(oriFiles);
			
		// Buat data.txt
      		if ((!(strstr(dp->d_name, "_"))) && (strstr(dp->d_name, ".png"))) {  // untuk 1 poster
			FILE *fptr;
			char fname[100];
			long size;
			strcpy(fname, mkFolder);
			strcat(fname, "/data.txt");
			fptr = fopen(fname, "r");
			// cek apakah 
			if (fptr){
				fseek(fptr, 0, SEEK_END);
				size = ftell(fptr);	// cek size file txt nya
				fclose(fptr);
			}
			fptr = fopen(fname, "a+");
			if(size == 0){		// jika size nya 0, berarti data masih kosong
				fprintf(fptr, "kategori : %s\n\n", getKategori);
			}
			fprintf(fptr, "nama : %s\n", getJudul);
			fprintf(fptr, "rilis : tahun %s\n\n", getTahun);
			fclose(fptr);
		}
		
		// entah kenapa untuk yang 2 poster saya kena segmentation fault
/*
		else if(strstr(dp->d_name, "_") && (strstr(dp->d_name, ".png"))) {
		        FILE *fptr;
			char fname[100];
			strcpy(fname, oriFiles);
			strcat(fname, getKategori2);
			strcat(fname, "/data.txt");
			fptr = fopen(fname, "a+");
			fprintf(fptr, "nama : %s\n", getJudul2);
			fprintf(fptr, "rilis : tahun %s\n\n", getTahun2);
			fclose(fptr);
				
			strcpy(fname, oriFiles);
			strcat(fname, getKategori3);
			strcat(fname, "/data.txt");
			fptr = fopen(fname, "a+");
			fprintf(fptr, "nama : %s\n", getJudul3);
			fprintf(fptr, "rilis : tahun %s\n\n", getTahun3);
			fclose(fptr);
		}

*/		

            // Construct new path from our base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);
            
            listFilesRecursively(path);
        }
    }

    closedir(dir);
}

int main (){

	makedir("/home/reyner/shift2/drakor");
	unzip("/home/reyner/shift2/drakor");
	char path[100]="/home/reyner/shift2/drakor";


    	listFilesRecursively(path);
    	return 0;

}
